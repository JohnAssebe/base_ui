import { createRouter, createWebHistory } from "vue-router";
import PostProblem from "../views/PostTicket.vue";
import Home from "../components/Home.vue";
import SignUp from "../components/SignUp.vue";
import AdminDashboard from "../components/dash.vue" 
const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta:{title:'Home'}
  },
  {
    path: "/postproblem",
    name: "PostProblem",
    component: PostProblem,
    meta:{title:'Post'}
  },  
  {
    path: "/admin",
    name: "Admin",
    component: AdminDashboard,
    meta:{title:'AdminDashboard'}
  },
  {
    path: "/signup",
    name: "SignUp",
    component: SignUp,
    meta:{title:'Signup'}
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});
router.beforeEach((to,from,next)=>{
  document.title=to.meta.title
  next()
});
export default router;
